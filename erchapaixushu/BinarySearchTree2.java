package erchapaixushu;

public class BinarySearchTree2{
    private Node root;
    private int size;
    /*******/
    public BinarySearchTree2(Node root){
        this.root=root;
        size++;
    }
    /***********/
    public int getSize(){
        return this.size;
    }
    /***********/
    public boolean contains(Node data){
        return contains(this.root);
        //return false;
    }
    /*************/
    boolean contains(Data n, Node root){
        if(root==null){
            return false;
        }
        int compare=n.compareTo(root.element);
        if(compare>0){
            if(root.right!=null){
                return contains(n,root.right);
            }else{
                return false;
            }
        }else if(compare<0){
            if(root.left!=null){
                return contains(n,root.left);
            }else{
                return false;
            }
        }else{
            return true;
        }
    }
    /***************/
    public boolean insert(Data n){
        boolean flag = insert(n,this.root);
        if(flag) size++;
        return flag;
    }
    /***************/
    private boolean insert(Data n, Node root){
        if(root==null){
            this.root=new Node(n);
            return true;
        }else if(root.element.compareTo(n)>=0){
            if(root.left!=null){
                return insert(n,root.left);
            }else{
                root.left=new Node(n);
                return true;
            }
        }else if(root.element.compareTo(n)<0){
            if(root.right!=null){
                return insert(n,root.right);
            }else{
                root.right=new Node(n);
                return true;
            }
        }else{
            root.frequency++;
            return true;
        }
    }
    /*********************/
    public boolean remove(Data name){
        root = remove(name,this.root);
        if(root != null){
            size--;
            return true;
        }
        return false;
    }
    /******************/
    private Node remove(Data name, Node root){
        int compare = root.element.compareTo(name);
        if(compare == 0){
            if(root.frequency>1){
                root.frequency--;
            }else{
                /**根据删除节点的类型，分成以下几种情况
                 **如果被删除的节点是叶子节点，直接删除
                 **如果被删除的节点含有一个子节点，让指向该节点的指针指向他的儿子节点
                 **如果被删除的节点含有两个子节点，找到左字数的最大节点，并替换该节点
                 **/
                if(root.left == null && root.right == null){
                    root = null;
                }else if(root.left !=null && root.right == null){
                    root = root.left;
                }else if(root.left == null && root.right != null){
                    root = root.right;
                }else{
                    //被删除的节点含有两个子节点
                    Node newRoot = root.left;
                    while (newRoot.left != null){
                        newRoot = newRoot.left;//找到左子树的最大节点
                    }
                    root.element = newRoot.element;
                    root.left = remove(root.element,root.left);
                }
            }
        }else if(compare > 0){
            if(root.left != null){
                root.left = remove(name,root.left);
            }else{
                return null;
            }
        }else{
            if(root.right != null){
                root.right = remove(name,root.right);
            }else{
                return null;
            }
        }
        return root;
    }
    /*********************/
    public String toString(){
        //中序遍历就可以输出树中节点的顺序
        return toString(root);
    }
    /*******************/
    private String toString(Node n){
        String result = "";
        if(n != null){
            if(n.left != null){
                result += toString(n.left);
            }
            result += n.element + " ";
            if(n.right != null){
                result += toString(n.right);
            }
        }
        return result;
    }
/*******************/
}