package erchapaixushu;

import java.util.Scanner;

public class BinarySearchTreeTest2 {
    public static void main(String[] args){
/**********************/
        Node root = new Node(new Data(10));
        BinarySearchTree2 bst =new BinarySearchTree2(root);
        bst.insert(new Data(18));
        bst.insert(new Data(3));
        bst.insert(new Data(8));
        bst.insert(new Data(12));
        bst.insert(new Data(2));
        bst.insert(new Data(7));
        bst.insert(new Data(3));
        System.out.print("中序遍历结果为： ");
        System.out.println(bst);
        System.out.println();
        //查找1
        System.out.println("你想查找的数为：");
        Scanner scan=new Scanner(System.in);
        Data target=new Data();
        Integer data=scan.nextInt();
        target.data(data);
        boolean ifExist=bst.contains(target,root);
        System.out.println(data+"是否存在？");
        System.out.println(ifExist);
        //查找2
        System.out.println("你想查找的数为：");
        Data target2=new Data();
        Integer data2=scan.nextInt();
        target2.data(data2);
        boolean ifExist2=bst.contains(target2,root);
        System.out.println(data2+"是否存在？");
        System.out.println(ifExist2);
        //插入1
        System.out.println("你想插入的数是： ");
        Integer data3=scan.nextInt();
        bst.insert(new Data(data3));
        System.out.print("中序遍历结果为： ");
        System.out.println(bst);
        //插入2
        System.out.println("你想插入的数是： ");
        Integer data4=scan.nextInt();
        bst.insert(new Data(data4));
        System.out.print("中序遍历结果为： ");
        System.out.println(bst);
        //删除
        System.out.println("删除叶子节点2：");
        bst.remove(new Data(2));
        System.out.println(bst);
        System.out.println("删除只有一个子节点的节点7：");
        bst.remove(new Data(7));
        System.out.println(bst);
        System.out.println("删除含两个字节点的节点18");
        bst.remove(new Data(18));
        System.out.println(bst);
    }
}