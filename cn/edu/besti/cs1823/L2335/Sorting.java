package cn.edu.besti.cs1823.L2335;

public class Sorting {
    protected int[] num;
    public Sorting(int[] a){
        num=a;
    }
    public void SelectionSort(){
        int temp;
        for(int i=0;i<num.length;i++){
            int min=i;
            for(int j=i;j<num.length;j++){
                if(num[j]<num[min]){
                    min=j;
                }
            }
            temp=num[min];
            num[min]=num[i];
            num[i]=temp;
        }
    }
    public String toString() {
        String result = "";
        for (int i = 0; i < num.length; i++) {
            result += (num[i]+"  ");
        }
        return result;
    }
}