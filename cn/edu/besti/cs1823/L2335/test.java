package cn.edu.besti.cs1823.L2335;

public class test {
    public static void main(String[] args){
        int[] a={1,2,3,4,5,6,7};
        int[] b={5,4,6,1,2,3,9};
        int[] c={7,6,5,4,3,2,1};
        Searching s=new Searching(a);
        Searching s1=new Searching(b);
        System.out.println("找到为true，找不到为false:  ");
        System.out.println(s.LinearSearch(1));
        System.out.println(s.LinearSearch(5));
        System.out.println(s.LinearSearch(7));
        System.out.println(s.LinearSearch(13));
        System.out.println(s1.LinearSearch(1));
        System.out.println(s1.LinearSearch(5));
        System.out.println(s1.LinearSearch(9));
        System.out.println(s1.LinearSearch(13));
        Sorting s2=new Sorting(a);
        Sorting s3=new Sorting(b);
        Sorting s4=new Sorting(c);
        System.out.println("选择排序结果为： ");
        s2.SelectionSort();
        s3.SelectionSort();
        s4.SelectionSort();
        System.out.println(s2);
        System.out.println(s3);
        System.out.println(s4);
    }
}