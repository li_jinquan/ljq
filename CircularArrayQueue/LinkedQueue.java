package CircularArrayQueue;

public class LinkedQueue<T> implements ADT {

    private int count;

    private Node<T> head,tail;
    public LinkedQueue()
    {
        count = 0;
        head = tail = null;
    }

    @Override
    public void enqueue(Object element) {
        Node<T> node = new Node<T>((T) element);
        if (head == null) {
            head = node;
        } else {
            Node temp = head;
            while (temp.getNext() != null) {
                temp = temp.getNext();
            }
            tail = node;
            count++;
        }
    }

    public void enquene(T element){
        Node<T> node = new Node<T>(element);
        if(count == 0)
            head = node;
        else
            tail.setNext(node);
        tail = node;
        count++;
    }

    @Override
    public T dequeue() {
        if(isEmpty())
            return null;

        T result = head.getElement();
        head = head.getNext();
        count--;

        if(isEmpty())
            tail = null;
        return null;
    }

    @Override
    public T first() {
        System.out.println(head.getElement());
        return null;
    }

    @Override
    public boolean isEmpty() {
        return count == 0 ? true:false;
    }

    @Override
    public int size() {
        return count;
    }

    public boolean prinless(){
        Node<T> temp = head;
        temp.setNext(head.getNext());
        while (temp != null)
        {
            System.out.print("  " + temp.getElement());
//            System.out.println("123");
            temp = temp.getNext();
        }
        return true;
    }
}