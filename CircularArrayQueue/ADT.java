package CircularArrayQueue;

public interface ADT <T>{
    public void enqueue(T element);

    public T dequeue();

    public T first();

    public boolean isEmpty();

    public int size();

    public String toString();
}