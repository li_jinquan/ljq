import java.io.*;

public class FileTest1 {
    public static void main(String[] args) throws IOException {
        File file = new File("Test1.txt");
        if (!file.exists()){
            file.createNewFile();
        }
        OutputStream outputStream1 = new FileOutputStream(file);
        byte[] test1 = {'T','e','s','t','1','!'};
        outputStream1.write(test1);

        InputStream inputStream1 = new FileInputStream(file);
        while (inputStream1.available()> 0){
            System.out.print((char) inputStream1.read()+"  ");
        }
        inputStream1.close();
    }
}
