import java.util.Random;
import java.text.DecimalFormat;

public class random
{
   public static void main (String[] args)
   {
      float b;
      Random a = new Random();
      b = a.nextFloat()*20-10;

      DecimalFormat fmt = new DecimalFormat("0.###");
      
      System.out.println("From -10 to 10 : " + fmt.format(b));
   }
}
