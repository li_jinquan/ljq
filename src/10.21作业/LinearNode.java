package ArrayStack;

public class LinearNode<T> {
    private LinearNode<T> next;
    private T element;
    /*Create an empty node.创造一个空结点。*/
    public LinearNode(){
        next = null;
        element = null;
    }
    /*Creats a node storing the specified element.创造一个结点储存特殊元素。
     * @param elem element to be stored
     * */

    public LinearNode (T elem) {
        next=null;
        element = elem;
    }
    /*Returns the node that follows this one.//返回节点后面的一个链表元素。
     * @return LinearNode<T> reference to next node*/
    public LinearNode<T> getNext(){
        return next;
    }
    /*Sets the node that follows this one.//将结点后面的串在当前结点后面。
     * @param node nnode to follow this one */
    public void setNext(LinearNode<T> node)
    {
        next=node;
    }
    /*Returns the element stored in this node.//返回结点处的数值。
     * @return  T element stored at the node.*/
    public T getElement(){
        return element;
    }
    /*Sets the element stored in the node.//设置元素值的方法。
     * @param elem element to be stored at thhis node*/
    public void setElement(T elem)
    {
        element= elem;
    }
}