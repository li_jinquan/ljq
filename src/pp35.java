import java.util.Scanner;

import java.text.DecimalFormat;



public class pp35

{

  public static void main (String[] args)

  {     

       double radius;

       double area,volume;

     

       Scanner scan = new Scanner (System.in);

      

       System.out.print ("Enter the radius of circle: ");

       radius =scan.nextDouble();

       

       area = 4 * Math.PI * Math.pow(radius,2);

       volume = 4/3 * Math.PI * Math.pow(radius,3);

       

       DecimalFormat fmt = new DecimalFormat ("0.####");

       

       System.out.println ("THe Superficial Area: " + fmt.format(area));

       System.out.println ("The Volume: " + fmt.format(volume));

   }

}
