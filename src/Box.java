public class Box{
    private double height, weight, depth;
    private boolean full=false;
    public Box(double height,double weight,double depth){
        this.depth = depth;
        this.height = height;
        this.weight = weight;
    }
    public void setFull(boolean full){
        this.full = full;
    }
    public void setDepth(double depth){
        this.depth = depth;
    }
    public void setHeight(double height){
        this.height = height;
    }
    public void setWeight(double weight){
        this.weight = weight;
    }
    public String toString(){
        String str = "Depth:";
        str += this.depth + "Weight:";
        str += this.weight +  "Height:";
        str += this.height + "Full:";
        str += this.full;
        return str;
    }
}