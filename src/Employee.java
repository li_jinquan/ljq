package PP92;//  Employee.java       Java Foundations
//
//  Represents a general paid employee.
//********************************************************************

public class Employee extends StaffMember
{
    protected String socialSecurityNumber;
    protected double payRate;

    //-----------------------------------------------------------------
    //  Constructor: Sets up this employee with the specified
    //-----------------------------------------------------------------
    public Employee (String eName, String eAddress, String ePhone,int eGrade,
                     String socSecNumber, double rate)
    {
        super (eName, eAddress, ePhone, eGrade);

        socialSecurityNumber = socSecNumber;
        payRate = rate;
    }

    //-----------------------------------------------------------------
    //  Returns information about an employee as a string.finshing
    //-----------------------------------------------------------------
    public String toString()
    {

        String result = String.format("%s%s", super.toString(), "\nSocial Security Number: " + socialSecurityNumber);

        return result;
    }

    //-----------------------------------------------------------------
    //  ljq 20182335
    //-----------------------------------------------------------------
    public double pay()
    {
        return payRate;
    }

    @Override
    public void holiday() {
        int op = grade;
        switch (op) {
            case 1:
                System.out.println("Holiday: 10");
                break;
            case 2:
                System.out.println("Holiday: 9");
                break;
            case 3:
                System.out.println("Holiday: 8");
                break;
            case 4:
                System.out.println("Holiday: 7");
                break;
            default:
                break;
        }
    }
}