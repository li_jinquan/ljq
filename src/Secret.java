package PP94;

import PP92.Staff;

import java.util.Random;

public class Secret extends Staff {
    private String message;
    private boolean encrypted;
    private int shift;
    private Random generator;
    public Secret(String mes){
        message=mes;
        encrypted=false;
        generator=new Random();
        shift=generator.nextInt(10)+5;
    }
    @Override
    public void encrypt() {
        if(!encrypted){
            String masked="";
            for(int index=0;index<message.length();index++)
            {
                masked+=(char)(message.charAt(index)+shift);
                message=masked;
                encrypted=true;
            }
        }
    }

    @Override
    public String decrypt() {
        if(encrypted){
            String unmasked="";
            for(int index=0;index<message.length();index++)
            {
                unmasked+=(char)(message.charAt(index)-shift);
                message=unmasked;
                encrypted=true;
            }
        }
        return message;
    }
    public String toString(){
        return message;
    }
}