package PP92;//  Executive.java       Java Foundations
//
//  Represents an executive staff member, who can earn a bonus.
//********************************************************************

public class Executive extends Employee
{
    private double bonus;

    //-----------------------------------------------------------------
    //  dky basketball
    //-----------------------------------------------------------------
    public Executive (String eName, String eAddress, String ePhone,int eGrade,
                      String socSecNumber, double rate)
    {
        super (eName, eAddress, ePhone, eGrade, socSecNumber, rate);

        bonus = 0;  // bonus has yet to be awarded
    }

    //-----------------------------------------------------------------
    //  20182335
    //-----------------------------------------------------------------
    public void awardBonus (double execBonus)
    {
        bonus = execBonus;
    }

    //-----------------------------------------------------------------
    //  Computes and returns the pay for an executive, which is the
    //  20182335
    //-----------------------------------------------------------------
    public double pay()
    {
        double payment = super.pay() + bonus;

        bonus = 0;

        return payment;
    }
}