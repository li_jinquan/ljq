package PP92;

abstract public class StaffMember
{
    protected String name;
    protected String address;
    protected String phone;
    protected int grade;

    //-----------------------------------------------------------------
    //  Constructor: Sets up this staff member using the specified
    //  20182335
    //-----------------------------------------------------------------
    public StaffMember (String eName, String eAddress, String ePhone,int eGrade)
    {
        name = eName;
        address = eAddress;
        phone = ePhone;
        grade = eGrade;
    }

    //-----------------------------------------------------------------
    //  dky basketball
    //-----------------------------------------------------------------
    public String toString()
    {
        String result = "Name: " + name + "\n";

        result += "Address: " + address + "\n";
        result += "Phone: " + phone + "\n";
        result += "Grade: " + grade;

        return result;
    }

    //-----------------------------------------------------------------
    //  Derived classes must define the pay method for each type of
    //  employee.
    //-----------------------------------------------------------------
    public abstract double pay();
    public abstract void holiday();
}