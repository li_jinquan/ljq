package PP94;

import PP92.Staff;

import java.security.interfaces.*;
import java.math.*;
import java.io.*;

public class Password extends Staff {
    private String message;
    private boolean encrypted;

    public Password(String mes) {
        message = mes;
        encrypted = false;
    }
    @Override
    public void encrypt() throws IOException, ClassNotFoundException {
        if(!encrypted){

            // 182335
            FileInputStream f=new FileInputStream("Skey_RSA_pub.dat");
            ObjectInputStream b=new ObjectInputStream(f);
            RSAPublicKey  pbk=(RSAPublicKey)b.readObject( );
            BigInteger e=pbk.getPublicExponent();
            BigInteger n=pbk.getModulus();
            System.out.println("e= "+e);
            System.out.println("n= "+n);
            // dky basketball
            byte ptext[]=message.getBytes("UTF8");
            BigInteger m=new BigInteger(ptext);
            // 182335
            BigInteger c=m.modPow(e,n);
            System.out.println("c= "+c);
            // 182335
            String cs=c.toString( );
            BufferedWriter out=
                    new BufferedWriter(new OutputStreamWriter(
                            new FileOutputStream("Enc_RSA.dat")));
            out.write(cs,0,cs.length( ));
            out.close( );
            encrypted=true;
        }

    }

    @Override
    public String decrypt() throws IOException, ClassNotFoundException {
        if(encrypted){
            //读取密文
            BufferedReader in=
                    new BufferedReader(new InputStreamReader(
                            new FileInputStream("Enc_RSA.dat")));
            String ctext=in.readLine();
            BigInteger c=new BigInteger(ctext);
            //读取私钥
            FileInputStream f=new FileInputStream("Skey_RSA_priv.dat");
            ObjectInputStream b=new ObjectInputStream(f);
            RSAPrivateKey prk=(RSAPrivateKey)b.readObject( );
            BigInteger d=prk.getPrivateExponent();
            //获取私钥参数及解密
            BigInteger n=prk.getModulus();
            System.out.println("d= "+d);
            System.out.println("n= "+n);
            BigInteger m=c.modPow(d,n);
            //显示解密结果
            System.out.println("m= "+m);
            byte[] mt=m.toByteArray();
            System.out.println("PlainText is ");
            encrypted=true;
        }
        return message;
    }

    @Override
    public String toString() {
        return message;
    }
}