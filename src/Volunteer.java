package PP92;

public class Volunteer extends StaffMember
{
    //-----------------------------------------------------------------
    //  Constructor: Sets up this volunteer using the specified
    //  information.
    //-----------------------------------------------------------------
    public Volunteer (String eName, String eAddress, String ePhone,int eGrade)
    {
        super (eName, eAddress, ePhone,eGrade);
    }

    //-----------------------------------------------------------------
    //  Returns a zero pay value for this volunteer.
    //-----------------------------------------------------------------
    public double pay()
    {
        return 0.0;
    }

    @Override
    public void holiday() {
        int op = grade;
        switch (op) {
            case 1:
                System.out.println("Holiday: 10");
                break;
            case 2:
                System.out.println("Holiday: 9");
                break;
            case 3:
                System.out.println("Holiday: 8");
                break;
            case 4:
                System.out.println("Holiday: 7");
                break;
            default:
                break;
        }
    }
}
