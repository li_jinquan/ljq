package Huffman;

import java.io.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static Huffman.HuffmanTree.breadthFirstTraversal;
//20182335
public class HuffmanTest {
    public static void main(String[] args) throws IOException {
        File file = new File("D:\\text.txt");
        Reader reader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(reader);
        String temp = bufferedReader.readLine();

        char characters[] = new char[temp.length()];
        for (int i = 0; i < temp.length(); i++) {
            characters[i] = temp.charAt(i);
        }
        System.out.println("原字符集为：" + Arrays.toString(characters));
        double frequency[] = new double[27];
        int numbers = 0;
        for (int i = 0; i < characters.length; i++) {
            if (characters[i] == ' ') {
                numbers++;
            }
            frequency[26] = (float) numbers / characters.length;
        }
        System.out.println("字符集为");
        for (int j = 97; j <= 122; j++) {
            int number = 0;
            for (int m = 0; m < characters.length; m++) {
                if (characters[m] == (char) j) {
                    number++;
                }
                frequency[j - 97] = (float) number / characters.length;
            }
            System.out.print((char) j + "，");
        }
        System.out.println("空");
        System.out.println("每一个字符对应的概率为（从a到z还有一个空格）" + "\n" + Arrays.toString(frequency));
        double result = 0.0;
        for (int z = 0; z < 27; z++) {
            result += frequency[z];
        }
        System.out.println("总概率之和为" + result);
        List<Node> nodes = new ArrayList<Node>();
        for (int o = 97; o <= 122; o++) {
            nodes.add(new Node((char) o, frequency[o - 97]));
        }
        nodes.add(new Node(' ', frequency[26]));

        Node root = HuffmanTree.createTree(nodes);

        System.out.println("广度遍历下的哈夫曼树：");
        System.out.println(breadthFirstTraversal(root));
        String result1 = "";
        List<Node> temp1 = breadthFirstTraversal(root);

        for (int i = 0; i < characters.length; i++) {
            for (int j = 0; j < temp1.size(); j++) {

                if (characters[i] == temp1.get(j).getData()) {
                    result1 += temp1.get(j).getCodenumber();
                }
            }
        }
        System.out.println("对文件进行编码后的结果为：");
        System.out.println(result1);
        File file2 = new File("D:\\huffmantext.txt");
        Writer writer = new FileWriter(file2);
        writer.write(result1);
        writer.close();
        List<String> newlist = new ArrayList<>();
        for(int m=0;m < temp1.size();m++)
        {
            if(temp1.get(m).getData()!='无')
                newlist.add(String.valueOf(temp1.get(m).getData()));
        }
        System.out.println("字符："+newlist);

        List<String> newlist1 = new ArrayList<>();
        for(int m=0;m < temp1.size();m++)
        {
            if(temp1.get(m).getData()!='无')
                newlist1.add(String.valueOf(temp1.get(m).getCodenumber()));
        }
        System.out.println("对应编码："+newlist1);
        FileReader fileReader = new FileReader("D:\\huffmantext.txt");
        BufferedReader bufferedReader1 = new BufferedReader(fileReader);
        String secretline = bufferedReader1.readLine();
        List<String> secretText = new ArrayList<String>();
        for (int i = 0; i < secretline.length(); i++) {
            secretText.add(secretline.charAt(i) + "");
        }
        String result2 = "";
        String current="";
        while(secretText.size()>0) {
            current = current + "" + secretText.get(0);
            secretText.remove(0);
            for (int p = 0; p < newlist1.size(); p++) {
                if (current.equals(newlist1.get(p))) {
                    result2 = result2 + "" + newlist.get(p);
                    current="";
                }

            }
        }
//20182335
        System.out.println("解码后的结果："+result2);
        File file3 = new File("D:\\text1.txt");
        Writer writer1 = new FileWriter(file3);
        writer1.write(result2);
        writer.close();
    }
}