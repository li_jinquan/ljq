package Huffman;

public class Node implements Comparable<Node> {
    private char data;
    private double weight;
    private Node left;
    private Node right;
    String codenumber;

    public Node(char data, double weight){
        this.data = data;
        this.weight = weight;
        this.codenumber ="";
    }
     //dky20182335
    public char  getData() {
        return data;
    }

    public void setData(char data) {
        this.data = data;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }
    public String getCodenumber(){
        return codenumber;
    }

    public void setCodenumber(String number){
        codenumber = number;
    }
    @Override
    public String toString(){
        return "data:"+this.data+" weight:"+this.weight+" codenumber:"+this.codenumber+"\n\t";
    }
     //20182335
    @Override
    public int compareTo(Node other) {
        if(other.getWeight() > this.getWeight()){
            return 1;
        }
        if(other.getWeight() < this.getWeight()){
            return -1;
        }
        else
            return 0;
    }
}