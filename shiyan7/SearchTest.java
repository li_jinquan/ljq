package shiyan7;

import junit.framework.TestCase;
import org.junit.Test;

public class SearchTest extends TestCase {
    int[] a={1,2,3,4,5,6,7,8};
    int[] b={8,7,6,5,4,3,2,1,2335};
    int[] c={1,3,5,2,2,2,8,2,3,1,6};
    Searching s1=new Searching(a);
    Searching s2=new Searching(b);
    Searching s3=new Searching(c);

    @Test
    public void testLinearSearch() {
        assertEquals(true,s2.LinearSearch(3));
        assertEquals(true,s1.LinearSearch(1));
        assertEquals(true,s1.LinearSearch(8));
        assertEquals(false,s1.LinearSearch(12));
        assertEquals(true,s2.LinearSearch(1));
        assertEquals(true,s1.LinearSearch(8));
        assertEquals(false,s1.LinearSearch(12));
        assertEquals(true,s3.LinearSearch(3));
        assertEquals(true,s3.LinearSearch(1));
        assertEquals(true,s3.LinearSearch(8));
        assertEquals(true,s3.LinearSearch(2));
        assertEquals(false,s3.LinearSearch(12));
        assertEquals(false,s2.LinearSearch(2335));
    }
//    @Test
//    public void testBinarySearch() {
//        assertEquals(true,s1.BinarySearch(1));
//        assertEquals(false,s1.BinarySearch(12));
//        assertEquals(true,s1.BinarySearch(1));
//        assertEquals(false,s1.BinarySearch(12));
//        assertEquals(true,s3.LinearSearch(8));
//        assertEquals(true,s3.LinearSearch(1));
//        assertEquals(true,s3.LinearSearch(3));
//        assertEquals(false,s3.LinearSearch(12));
//
//    }
//    @Test
//    public void testDiguiBinary() {
//        assertEquals(true,s1.DiguiBinary(a,0,7,4));
//        assertEquals(true,s1.DiguiBinary(a,0,7,1));
//        assertEquals(true,s1.DiguiBinary(a,0,7,8));
//        assertEquals(false,s1.DiguiBinary(a,0,7,12));
//        assertEquals(true,s2.DiguiBinary(a,0,7,4));
//        assertEquals(true,s2.DiguiBinary(a,0,7,1));
//        assertEquals(true,s2.DiguiBinary(a,0,7,8));
//        assertEquals(false,s2.DiguiBinary(a,0,7,12));
//        assertEquals(true,s3.DiguiBinary(a,0,6,3));
//        assertEquals(true,s3.DiguiBinary(a,0,6,1));
//        assertEquals(false,s3.DiguiBinary(a,0,6,12));
//    }
//    @Test
//    public void testChazhi() {
//        assertEquals(true,s1.Chazhi(a,1,0,7));
//        assertEquals(true,s1.Chazhi(a,8,0,7));
//        assertEquals(true,s1.Chazhi(a,3,0,7));
//        assertEquals(false,s1.Chazhi(a,12,0,7));
//        assertEquals(true,s2.Chazhi(a,1,0,7));
//        assertEquals(true,s2.Chazhi(a,1,0,7));
//        assertEquals(true,s2.Chazhi(a,8,0,7));
//        assertEquals(true,s2.Chazhi(a,3,0,7));
//    }
//
//    @Test
//    public void testFibonacciSearch() {
//        assertEquals(true,s1.FibonacciSearch(a,8,2));
//        assertEquals(true,s1.FibonacciSearch(a,8,1));
//        assertEquals(true,s1.FibonacciSearch(a,8,8));
//        assertEquals(false,s1.FibonacciSearch(a,8,12));
//        assertEquals(true,s2.FibonacciSearch(a,8,2));
//        assertEquals(true,s2.FibonacciSearch(a,8,1));
//        assertEquals(true,s2.FibonacciSearch(a,8,8));
//        assertEquals(false,s2.FibonacciSearch(a,8,12));
//
//    }
//
//    public void testHashLinked() {
//        assertEquals(true,s1.FibonacciSearch(a,8,3));
//        assertEquals(true,s1.FibonacciSearch(a,8,1));
//        assertEquals(true,s1.FibonacciSearch(a,8,8));
//        assertEquals(false,s1.FibonacciSearch(a,8,12));
//        assertEquals(true,s2.FibonacciSearch(a,8,3));
//        assertEquals(true,s2.FibonacciSearch(a,8,1));
//        assertEquals(true,s2.FibonacciSearch(a,8,8));
//        assertEquals(false,s2.FibonacciSearch(a,8,12));
//
//    }
}