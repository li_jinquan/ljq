package shiyan7;

public class Searching {
    protected static int[] num;
    public Searching(int[] a){
        num=a;
    }


    // 线性查找****
    public static boolean LinearSearch(int a){
        for(int i=0;i<num.length;i++){
            if(num[i]==a){
                return true;
            }
        }
        return false;
    }

    // 普通****
    public static boolean BinarySearch(int a){
        int temp=a;
        int low=0;
        int high=num.length-1;
        int mid=(high+low)/2;
        while(low<=high){
            if(num[mid]==temp){
                return true;
            }
            else if(temp>num[mid]){
                low=mid+1;
            }
            else{
                high=mid-1;
            }
            mid=(high+low)/2;
        }
        return false;
    }


    //递归****
    public boolean DiguiBinary(int[] data,int min, int max, int target){
        boolean found = false;
        int midpoint = (min + max) / 2;

        if (data[midpoint]==target)
            found = true;

        else if (data[midpoint]>target)
        {
            if (min <= midpoint - 1)
                found = DiguiBinary(data, min, midpoint - 1, target);
        }

        else if (midpoint + 1 <= max)
            found = DiguiBinary(data,midpoint + 1, max, target);

        return found;
    }


    //插值查找****
    public boolean Chazhi(int a[], int value, int low, int high){
        int mid = (low+high)/2;
        while (low<=high) {
            if (a[mid] == value)
                return true;
            if (a[mid] > value)
                return Chazhi(a, value, low, mid - 1);
            if (a[mid] < value)
                return Chazhi(a, value, mid + 1, high);
        }
        return false;
    }




    //

    int max_size=20;

    void Fibonacci(int[] F)
    {
        F[0]=0;
        F[1]=1;
        for(int i=2;i<max_size;++i)
            F[i]=F[i-1]+F[i-2];
    }

    public boolean FibonacciSearch(int[] a, int n, int key)  //a为要查找的数组,n为要查找的数组长度,key为要查找的关键字
    {
        int low=0;
        int high=n-1;

        int[] F=new int[max_size];
        Fibonacci(F);

        int k=0;
        while(n>F[k]-1)
            ++k;

        int[] temp;//将数组a扩展到F[k]-1的长度
        temp=new int [F[k]-1];
        for(int j=0;j<n;j++){
            temp[j]=a[j];
        }

        for(int i=n;i<F[k]-1;++i)
            temp[i]=a[n-1];

        while(low<=high)
        {
            int mid=low+F[k-1]-1;
            if(key<temp[mid])
            {
                high=mid-1;
                k-=1;
            }
            else if(key>temp[mid])
            {
                low=mid+1;
                k-=2;
            }
            else
            {
                if(mid<n)
                    return true; //若相等则说明mid即为查找到的位置
                else
                    return false; //若mid>=n则说明是扩展的数值,返回n-1
            }
        }
        return false;
    }

    // 二叉树****
    public static boolean search(int ele){
        int temp=ele;
        int low=0;
        int high=num.length-1;
        int mid=(high+low)/2;
        while(low<=high){
            if(num[mid]==temp){
                return true;
            }
            else if(temp>num[mid]){
                low=mid+1;
            }
            else{
                high=mid-1;
            }
            mid=(high+low)/2;
        }
        return false;

    }



    // 哈希值查找****
    private int addr;
    private int count=0;
    final int MAXSIZE=11;
    protected Number[] ele=new Number[11];
    final int Initial=0;
    public void HashLinked(int[] key){
        for(int i=0;i<ele.length;i++){
            ele[i]=new Number(0);
        }
        for(int j=0;j<ele.length;j++){
            Insert(num[j]);
        }
    }

    public void Insert(int key){
        int addr=hash(key);
        while(ele[addr]==null){
            ele[addr]=new Number(key);
        }
        Number temp=ele[addr];
        Number point=new Number(key);
        while(temp.next!=null){
            temp=temp.next;
        }
        if(temp.num==0){
            temp.num=point.num;
        }
        else
            temp.next=point;
        count++;
    }
    public int hash(int key){
        return key%MAXSIZE;
    }

    public boolean search1(int elem){
        int addr = hash(elem);
        while(ele[addr].num != elem) {
            addr = hash(addr + 1);
            if(ele[addr].num == Initial || addr == hash(elem)) {
                return false;
            }
        }
        return true;
    }
    public String toString(){
        String result="";
        String result2="";
        for(int i=0;i<MAXSIZE;i++){
            result+=(i+"  "+ele[i].toString()+"\n");
        }
        return result;
    }



}