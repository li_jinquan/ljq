package shiyan7;

import junit.framework.TestCase;

public class SortTest extends TestCase {

        int[] a={1,2,3,4,5,6,7,8};
        int[] b={8,7,6,5,4,3,2,1};
        int[] c={2,0,1,8,2,3,1,6};
        Sorting s1=new Sorting(a);
        Sorting s2=new Sorting(b);
        Sorting s3=new Sorting(c);
        public void testerchashuSort() {
            assertEquals("1 2 3 4 5 6 7 8 ",s1.SelectionSort());
            assertEquals("1 2 3 4 5 6 7 8 ",s2.SelectionSort());
            assertEquals("0 1 1 2 2 3 6 8 ",s3.SelectionSort());

        }

        public void testShellSort() {
            assertEquals("1 2 3 4 5 6 7 8 ",s1.shellSort());
            assertEquals("1 2 3 4 5 6 7 8 ",s2.shellSort());
            assertEquals("0 1 1 2 2 3 6 8 ",s3.shellSort());
        }

        public void testduisort(){
            assertEquals("1 2 3 4 5 6 7 8 ",s1.duisort());
            assertEquals("1 2 3 4 5 6 7 8 ",s2.duisort());
            assertEquals("0 1 1 2 2 3 6 8 ",s3.duisort());

}}