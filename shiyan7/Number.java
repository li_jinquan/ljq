package shiyan7;

public class Number {
    protected int num;
    public Number next=null;
    public Number(int a){
        num=a;
    }
    @Override
    public String toString() {
        while (num != 0) {
            return num + "->" + next;
        }
        return "";
    }
}