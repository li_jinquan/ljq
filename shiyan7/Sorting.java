package shiyan7;

public class Sorting {
    protected int[] num;

    public Sorting(int[] a) {
        num = a;
    }

    public String SelectionSort() {
        int temp;
        for (int i = 0; i < num.length; i++) {
            int min = i;
            for (int j = i; j < num.length; j++) {
                if (num[j] < num[min]) {
                    min = j;
                }
            }
            temp = num[min];
            num[min] = num[i];
            num[i] = temp;
        }
        String result="";
        for(int m=0;m<num.length;m++){
            result+=(num[m]+" ");
        }
        return result;
    }

    public  String shellSort() {
        int N = num.length;

        for (int h = N / 2; h > 0; h /= 2) {//希尔增量*****
            for (int i = h; i < N; i++) {
                //将a[i]插入到a[i-h],a[i-2h],a[i-3h]...中
                for (int j = i; j >= h && num[j] < num[j - h]; j -= h) {
                    int temp = num[j];
                    num[j] = num[j - h];
                    num[j - h] = temp;
                }
            }
        }
        String result="";
        for(int m=0;m<num.length;m++){
            result+=(num[m]+" ");
        }
        return result;
    }

    public String duisort() {

        for (int i =num.length - 1; i > 0; i--) {
            max_heapify(num, i);

            //堆顶元素(第一个元素)与Kn交换*****
            int temp = num[0];
            num[0] = num[i];
            num[i] = temp;
        }
        String result="";
        for(int m=0;m<num.length;m++){
            result+=(num[m]+" ");
        }
        return result;
    }


    public static void max_heapify(int[] a, int n) {
        int child;
        for (int i = (n - 1) / 2; i >= 0; i--) {
            //左子节点位置
            child = 2 * i + 1;
            //右子节点存在且大于左子节点，child变成右子节点*****
            if (child != n && a[child] < a[child + 1]) {
                child++;
            }
            //交换父节点与左右子节点中的最大值*****
            if (a[i] < a[child]) {
                int temp = a[i];
                a[i] = a[child];
                a[child] = temp;
            }
        }
    }
}