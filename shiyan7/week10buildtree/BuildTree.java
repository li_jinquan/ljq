package week10buildtree;

public class BuildTree {
    private BuildTree left;
    private BuildTree right;
    private BuildTree root;
    private Object data;
    public BuildTree(Object data, BuildTree left, BuildTree right){
        this.data = data;
        this.left = left;
        this.right = right;
    }

    public BuildTree(BuildTree data){
        this(data,null,null);
    }

    public BuildTree() {

    }

    public BuildTree(Object obj) {
    }


    public class TreeNode {
    }
}