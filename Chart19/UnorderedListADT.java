package Chart19;

import java.util.Iterator;

public interface UnorderedListADT<T> {
    Iterator<T> iterator();
}
