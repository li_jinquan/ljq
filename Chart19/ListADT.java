package Chart19;

public interface ListADT<T> {
    T remove(T element);

    boolean contains(T target);

    T removeLast() throws shiyan81_3.EmptyCollectionException;

    T removeFirst() throws shiyan81_3.EmptyCollectionException;

    int size();

    T first() throws shiyan81_3.EmptyCollectionException;

    T last() throws shiyan81_3.EmptyCollectionException;

    boolean isEmpty();
}
