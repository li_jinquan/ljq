package Chart19;

import java.util.Iterator;

public interface GraphADT<T> {
    void addEdge(T vertex1, T vertex2);

    void removeEdge(T vertex1, T vertex2);

    void addVertex(T vertex);

    void removeVertex(T vertex);

    Iterator<T> iteratorDFS(T startVertex);

    Iterator<T> iteratorBFS(T startVertex);

    Iterator<T> iteratorShortestPath(T startVertex, T targetVertex);

    int size();

    boolean isEmpty();

    boolean isConnected();
}
