package Chart19;

import java.io.Serializable;

public class Node<T> implements Serializable {
    protected int count;
    protected Node next, previous;

    public Node(int count, Node next, Node previous) {
        super();
        this.count = count;
        this.next = next;
        this.previous = previous;
    }

    public int getCount() {
        return count;
    }

    public Node getNext() {
        return next;
    }

    public Node getPrevious() {
        return previous;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public void setPrevious(Node previous) {
        this.previous = previous;
    }
}