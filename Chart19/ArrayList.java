package Chart19;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public abstract class ArrayList<T> implements ListADT<T>,Iterable<T> {
    private final static int DEFAUT_CAPACITY=100;
    private final static int NOT_FOUND=-1;

    protected int rear;
    protected T[] list;
    protected int modCount;
    protected T[] a;

    public ArrayList(){
        this(DEFAUT_CAPACITY);
    }

    public ArrayList(int initialCapacity)
    {
        rear = 0;
        list = (T[])(new Object[initialCapacity]);
        modCount = 0;
        a = (T[])(new Object[initialCapacity]);
    }

    @Override
    public T remove(T element)
    {
        T result;
        int index = find(element);

        if (index == NOT_FOUND)
            throw new shiyan81_3.ElementNotFoundException("ArrayList");

        result = list[index];
        rear--;

        // shift the appropriate elements
        for (int scan=index; scan < rear; scan++)
            list[scan] = list[scan+1];

        list[rear] = null;
        modCount++;

        return result;
    }

    private int find(T target)
    {
        int scan = 0;
        int result = NOT_FOUND;

        if (!isEmpty())
            while (result == NOT_FOUND && scan < rear)
                if (target.equals(list[scan]))
                    result = scan;
                else
                    scan++;

        return result;
    }

    @Override
    public boolean contains(T target)
    {
        return (find(target) != NOT_FOUND);
    }

    public void expandCapacity(){
        T[] larger = (T[]) new Object[list.length*2];
        for (int scan = 0;scan<rear;scan++){
            larger[scan]=list[0];

        }
        list=larger;


    }

    @Override
    public T removeLast() throws shiyan81_3.EmptyCollectionException
    {   int i =0;
        while (list[i]!=null){
            i++;
        }
        list[i-1]=null;
        rear--;
        return null;
    }

    @Override
    public T removeFirst() throws shiyan81_3.EmptyCollectionException
    {

        for (int i =0;i<size();i++){
            a[i]=list[i+1];
        }
        list=a;

        rear--;
        return null;
    }
    @Override
    public int size()
    {

        return rear;
    }
    @Override
    public T first() throws shiyan81_3.EmptyCollectionException
    {
        // To be completed as a Programming Project
        return list[0];
    }
    @Override
    public T last() throws shiyan81_3.EmptyCollectionException
    {
        int i =0;
        while (list[i]!=null){
            i++;
        }
        return list[i-1];
    }
    @Override
    public boolean isEmpty()
    {
        if (list[0]==null){
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString()
    {
        String result="";
        for (int i =0;i<size();i++){
            result+=list[i]+" "+"\n";
        }
        return result;
    }

    @Override
    public Iterator<T> iterator()
    {
        return new ArrayListIterator();
    }

    public abstract void addToFront(T element);

    public abstract void addAfter(T element, T target);

    /**
     * ArrayListIterator iterator over the elements of an ArrayList.
     */
    private class ArrayListIterator implements Iterator<T>
    {
        int iteratorModCount;
        int current;

        /**
         * Sets up this iterator using the specified modCount.
         *
         */
        public ArrayListIterator()
        {
            iteratorModCount = modCount;
            current = 0;
        }

        /**
         * Returns true if this iterator has at least one more element
         * to deliver in the iteration.
         *
         * @return  true if this iterator has at least one more element to deliver
         *          in the iteration
         * @throws ConcurrentModificationException if the collection has changed
         *          while the iterator is in use
         */
        @Override
        public boolean hasNext() throws ConcurrentModificationException
        {
            if (iteratorModCount != modCount)
                throw new ConcurrentModificationException();

            return (current < rear);
        }

        /**
         * Returns the next element in the iteration. If there are no
         * more elements in this iteration, a NoSuchElementException is
         * thrown.
         *
         * @return  the next element in the iteration
         * @throws NoSuchElementException if an element not found exception occurs
         * @throws  ConcurrentModificationException if the collection has changed
         */
        @Override
        public T next() throws ConcurrentModificationException
        {
            if (!hasNext())
                throw new NoSuchElementException();

            current++;

            return list[current - 1];
        }

        /**
         * The remove operation is not supported in this collection.
         *
         * @throws UnsupportedOperationException if the remove method is called
         */
        @Override
        public void remove() throws UnsupportedOperationException
        {
            throw new UnsupportedOperationException();
        }

    }
}
