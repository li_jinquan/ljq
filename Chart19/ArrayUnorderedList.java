package Chart19;

public class ArrayUnorderedList<T> extends ArrayList<T>
        implements UnorderedListADT<T>, Chart19.Undirected.UnorderedListADT<T> {

    public ArrayUnorderedList()
    {
        super();
    }


    public ArrayUnorderedList(int initialCapacity)
    {
        super(initialCapacity);
    }


    @Override
    public void addToFront(T element)
    {

        for (int i =0;i<size();i++){
            a[i]=list[i+1];
        }
        for (int i =0;i<size();i++){
            list[i+1]=a[i];
        }
        list[0]=element;
        rear++;
    }


    @Override
    public void addToRear(T element)
    {
        if (list[0]==null){
            list[0]=element;
        }
        else {
            int i =0;
            while (list[i]!=null){
                i++;
            }
            list[i]=element;
        }
        rear++;
    }


    @Override
    public void addAfter(T element, T target)
    {
        if (size() == list.length)
            expandCapacity();

        int scan = 0;

        // find the insertion point
        while (scan < rear && !target.equals(list[scan]))
            scan++;

        if (scan == rear)
            throw new shiyan81_3.ElementNotFoundException("UnorderedList");

        scan++;

        // shift elements up one
        for (int shift=rear; shift > scan; shift--)
            list[shift] = list[shift-1];

        // insert element
        list[scan] = element;
        rear++;
        modCount++;
    }
}